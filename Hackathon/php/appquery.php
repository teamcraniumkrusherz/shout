<?php
  // define variables and set to empty values
  include_once("mobiledatabase.php");
  $db = new mobiledatabase();

if ($_SERVER["REQUEST_METHOD"] == "POST") 
{
    $query_name = $_POST["query_name"];

    if($query_name === "signup")
    {
       $EMAIL = test_input ($_POST["EMAIL"]);
       $PASSWORD = test_input($_POST["PASSWORD"]);
       $NAME = test_input($_POST["NAME"]);
       $COMMUNITY_ID = test_input ($_POST["COMMUNITY_ID"]);
       $COMMUNITY_NAME = test_input($_POST["COMMUNITY_NAME"]);
       $SEX = test_input($_POST["SEX"]);
       $AGE = test_input($_POST["AGE"]);
       $STAR = 0;

       $db->signup($COMMUNITY_ID,$COMMUNITY_NAME,$NAME,$SEX,$AGE,$EMAIL,$PASSWORD,$STAR);
    }
    else if($query_name === "signin")
    {
      //echo "inside";
       $email = test_input ($_POST["EMAIL"]);
       $pass = test_input($_POST["PASSWORD"]);
       $db->signin($email, $pass);
    }

    else if($query_name === "getUserQuestion")
    {
      //echo "inside";
       $USER_ID = test_input ($_POST["USER_ID"]);
       $db->getUserQuestion($USER_ID);
    }

     else if($query_name === "getCommunityQuestion")
    {
      //echo "inside";
       $COMMUNITY_ID = test_input ($_POST["COMMUNITY_ID"]);
       $db->getCommunityQuestion($COMMUNITY_ID);
    }

    else if($query_name === "getAllAnswer")
    {
      //echo "inside";
       $QUESTION_ID = test_input ($_POST["QUESTION_ID"]);
       $db->getAllANswer($QUESTION_ID);
    }

    else if($query_name === "getAllCommunity")
    {
      //echo "inside";
    
       $db->getAllCommunity();
    }



    else if($query_name === "addQuestion")
    {
       $COMMUNITY_ID = test_input($_POST["COMMUNITY_ID"]);
       $USER_ID = test_input ($_POST["USER_ID"]);
       $DESCRIPTION = test_input($_POST["DESCRIPTION"]);
       $TIME = test_input ($_POST["TIME"]);
       $DATE = test_input($_POST["DATE"]);
       $LATITUDE = test_input ($_POST["LATITUDE"]);
       $LONGITUDE = test_input($_POST["LONGITUDE"]);
       $COMMUNITY_NAME = test_input ($_POST["COMMUNITY_NAME"]);
       $USER_NAME = test_input($_POST["USER_NAME"]);
       $STATUS=0;
       $db->addQuestion($COMMUNITY_ID,$USER_ID,$DESCRIPTION,$TIME,$DATE,$LATITUDE,$LONGITUDE,$COMMUNITY_NAME,$USER_NAME,$STATUS);
    }

    else if($query_name === "addAnswer")
    {
       $QUESTION_ID = test_input ($_POST["QUESTION_ID"]);
       
       $USER_ID = test_input ($_POST["USER_ID"]);
       $DESCRIPTION = test_input($_POST["DESCRIPTION"]);
       $TIME = test_input ($_POST["TIME"]);
       $DATE = test_input($_POST["DATE"]);
       $USER_NAME = test_input($_POST["USER_NAME"]);
       $STATUS=0;
       $db->addAnswer($QUESTION_ID,$USER_ID,$DESCRIPTION,$TIME,$DATE,$USER_NAME,$STATUS);
    }

	
	else if($query_name === "doneAnswer")
	{
		$ANSWER_ID = test_input ($_POST["ANSWER_ID"]);
		$USER_ID = test_input ($_POST["USER_ID"]);
		$STATUS=1;
		$db->doneAnswer($ANSWER_ID,$USER_ID,$STATUS);
		
	}
	
	else if($query_name === "updateFirebase")
	{
		$FIREBASE_ID = test_input ($_POST["FIREBASE"]);
		$USER_ID = test_input ($_POST["USER_ID"]);
		$db->updateFirebase($FIREBASE_ID,$USER_ID);
		
	}
















  


	else if($query_name === "totalCommunityUser")
	{
		$COMMUNITY_ID = test_input ($_POST["COMMUNITY_ID"]);
		$db->totalCommunityUser($COMMUNITY_ID);
		
	}
	
	else if($query_name === "pendingRequest")
	{
		$COMMUNITY_ID = test_input ($_POST["COMMUNITY_ID"]);		
		$db->pendingRequest($COMMUNITY_ID);
	}
	
  else
  {
    echo "no match";
  }

}
else
{
  echo "error";
}


function test_input($data) 
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>