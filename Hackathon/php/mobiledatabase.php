<?php

	class mobiledatabase extends SQLite3 
	{
        function __construct()
        {
            $this->open('data.db');
        }

       public function signup($COMMUNITY_ID,$COMMUNITY_NAME,$NAME,$SEX,$AGE,$EMAIL,$PASSWORD,$STAR)
        {
		    $sql = sprintf('INSERT INTO USER (COMMUNITY_ID,COMMUNITY_NAME,NAME,SEX,AGE,EMAIL, PASSWORD, STAR) VALUES ("%s", "%s", "%s", "%s","%s", "%s", "%s", "%s");',$COMMUNITY_ID,$COMMUNITY_NAME,$NAME,$SEX,$AGE,$EMAIL,$PASSWORD,$STAR);
		    $ret = $this->exec($sql);
		    if(!$ret)
		    {
		        echo "error"; 
		    }
		    else
		    {
		    	echo "success";
		    }
        }

        public function signin($EMAIL,$PASSWORD)
        {
        	$sql = sprintf('select * from USER where (EMAIL ="%s" and PASSWORD ="%s");', $EMAIL, $PASSWORD);
        	$this->printRes($sql);
        }


        public function addQuestion($COMMUNITY_ID,$USER_ID,$DESCRIPTION,$TIME,$DATE,$LATITUDE,$LONGITUDE,$COMMUNITY_NAME,$USER_NAME,$STATUS)
        {
        	$sql = sprintf('INSERT INTO QUESTION (COMMUNITY_ID,USER_ID,DESCRIPTION,TIMER,DATES,LATITUDE,LONGITUDE,COMMUNITY_NAME,USER_NAME,STATUS) VALUES ("%s", "%s", "%s", "%s","%s", "%s", "%s", "%s", "%s", "%s");',$COMMUNITY_ID,$USER_ID,$DESCRIPTION,$TIME,$DATE,$LATITUDE,$LONGITUDE,$COMMUNITY_NAME,$USER_NAME,$STATUS);
        	//echo "inserting";
        	$ret = $this->exec($sql);
		    if(!$ret)
		    {
		        echo "error"; 
		    }
		    else
		    {
		    	echo "success";
		    	$newsql = sprintf('Select * from USER where (COMMUNITY_ID="%s");',$COMMUNITY_ID);
		    	///$newret = $this->exec($newsql);
		    	$res2 =  $this->query($newsql);
	        	if(!$res2)
		        {
		      
	                echo $this->lastErrorMsg(); 
	            }
	            else
	            {
	            	
	            	while($row = $res2->fetchArray(SQLITE3_ASSOC))
		            {
		                  $data=  $row['firebase'];
		                  if (strlen($data)>25)
		                  {
		                  	$this->sendFCM("this is a message", $data);
		                  echo $data;		
		                  }
		                  
		            }
	    			///echo "HIII";
	            }

		    }
        }

        public function addAnswer($QUESTION_ID,$USER_ID,$DESCRIPTION,$TIME,$DATE,$USER_NAME,$STATUS)
        {
        	$sql = sprintf('INSERT INTO ANSWER (QUESTION_ID,USER_ID,DESCRIPTION,TIMER,DATES,USER_NAME,STATUS) VALUES ("%s", "%s", "%s", "%s","%s", "%s", "%s");',$QUESTION_ID,$USER_ID,$DESCRIPTION,$TIME,$DATE,$USER_NAME,$STATUS);
        	$ret = $this->exec($sql);
		    if(!$ret)
		    {
		        echo "error"; 
		    }
		    else
		    {
		    	echo "success";
		    }
        }

        public function getUserQuestion($USER_ID)
        {
        	$sql = sprintf('select * from QUESTION where (USER_ID ="%s");', $USER_ID);
        	$this->printRes($sql);
        }

        public function getAllAnswer($QUESTION_ID)
        {
        	$sql = sprintf('select * from ANSWER where (QUESTION_ID ="%s");', $QUESTION_ID);
        	$this->printRes($sql);
        }

 		public function getCommunityQuestion($COMMUNITY_ID)
        {
        	$sql = sprintf('select * from QUESTION where (COMMUNITY_ID ="%s");', $COMMUNITY_ID);
        	$this->printRes($sql);
        }

        public function getAllCommunity()
        {
        	$sql = sprintf('select * from COMMUNITY;');
        	$this->printRes($sql);
        }
		
		
		public function totalCommunityUser($COMMUNITY_ID)
        {
        	$sql = sprintf('select count(*) from USER where (COMMUNITY_ID = "%s");',$COMMUNITY_ID);
        	$this->printRes($sql);
        }

		
		public function pendingRequest($COMMUNITY_ID)
        {
        	$sql = sprintf('select count(*) from QUESTION where (COMMUNITY_ID = "%s" and STATUS = 0);',$COMMUNITY_ID);
        	$this->printRes($sql);
        }
		

		public function doneAnswer($ANSWER_ID,$USER_ID,$STATUS)
		{
			$sql = sprintf('UPDATE ANSWER SET STATUS = "1" where (ANSWER_ID = "%s");',$ANSWER_ID);
			//$this->printRes($sql);
			$ret = $this->exec($sql);
		    if(!$ret)
		    {
		        echo "error"; 
		    }
		    else
		    {
		    	echo "success";
				echo $USER_ID;
				$sql = sprintf('UPDATE USER SET STAR = (STAR + 1) where (USER_ID = "%s");',$USER_ID);
				$ret1 = $this->exec($sql);
				if(!$ret1)
				{
					echo "error"; 
				}
				else
					echo "success incrementing";
				
		    }
			
		}
		
		public function updateFirebase($FIREBASE_ID,$USER_ID)
		{
			$sql = sprintf('UPDATE USER SET firebase = "%s" where (USER_ID = "%s");',$FIREBASE_ID,$USER_ID);
			$ret1 = $this->exec($sql);
			if(!$ret1)
			{
				echo "error"; 
			}
			else
				echo "success";
		}
		
        public function printRes($sql){
		 $json = array();
            $res =  $this->query($sql);
            if(!$res){
                echo $this->lastErrorMsg(); 
            }
            else {
                while($row = $res->fetchArray(SQLITE3_ASSOC))
                {
                   
				array_push($json, $row);
                }
                
                echo json_encode($json);
            }
        }

        function sendFCM($mess,$id) {
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array (
			        'to' => $id,
			        'notification' => array (
			                "body" => $mess,
			                "title" => "Title",
			                "icon" => "myicon"
			        )
			);
			$fields = json_encode ( $fields );
			$headers = array (
			        'Authorization: key=>' . "AIzaSyAqtEioWrKVCI6UF559x6L_nuldnwHchhI",
			        'Content-Type: application/json'
			);

			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

			$result = curl_exec ( $ch );
			curl_close ( $ch );
			}

    }
?>

