package shout.hackathon.shout.Adapter;

/**
 * Created by AlZihad on 11/11/2016.
 */

/**
 * Created by AlZihad on 11/7/2016.
 */
        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;
        import java.util.ArrayList;
        import shout.hackathon.shout.Data;
        import shout.hackathon.shout.R;


public class HomeGridAdapter extends BaseAdapter
{

    private final Context mContext;
    // private final Book[] books;
    private final ArrayList<Data> books;
    // 1
    public HomeGridAdapter(Context context, ArrayList<Data> books) {
        this.mContext = context;
        this.books = books;
    }

    // 2
    @Override
    public int getCount() {
        return books.size();
    }

    // 3
    @Override
    public long getItemId(int position) {
        return position;
    }

    // 4
    @Override
    public Data getItem(int position) {
        return books.get(position);
    }

    // 5
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 1
        final Data data = books.get(position);
        // 2
        if (convertView == null)
        {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.rid_design, null);
        }
        // 3
        final ImageView imageView = (ImageView)convertView.findViewById(R.id.imageview_cover_art);
        final TextView nameTextView = (TextView)convertView.findViewById(R.id.textview_book_name);
        imageView.setImageResource(data.getImage());
        nameTextView.setText(data.getName());
        return convertView;
    }

}


/// AIzaSyD-2wwX0HnuhAyiJN7PtclFZEKuHA5G-YA