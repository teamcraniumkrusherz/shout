package shout.hackathon.shout.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import shout.hackathon.shout.CommunityData.Answers;
import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.DataHandler.Question;
import shout.hackathon.shout.R;

public class QuestionAdapter extends BaseAdapter{

    Context context;
    ArrayList<Question> questions;
    private static LayoutInflater inflater=null;
    public QuestionAdapter(Context mainActivity, ArrayList<Question> arrayList)
    {
        // TODO Auto-generated constructor stub

        context=mainActivity;
        this.questions = arrayList;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return questions.size();
    }

    @Override
    public Question getItem(int position) {
        // TODO Auto-generated method stub
        return questions.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView description;
        TextView questioner;
        TextView date;
        LinearLayout linear;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.question_data, null);
        holder.description=(TextView) rowView.findViewById(R.id.description);
        holder.questioner=(TextView) rowView.findViewById(R.id.name);
        holder.date=(TextView) rowView.findViewById(R.id.date);
        holder.linear=(LinearLayout) rowView.findViewById(R.id.linear);

        final Question ques = (Question) questions.get(position);
        holder.description.setText(ques.DESCRIPTION);

       /* holder.description.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"hiiiiiiiiiiiiii",Toast.LENGTH_LONG).show();
            }
        });*/
        holder.questioner.setText("Questioned by: "+ ques.USER_NAME);
        holder.date.setText("Date: "+ques.DATES);
        if (ques.STATUS.matches("0")||ques.STATUS.equals("0")|| ques.STATUS.contains("0") )
        {
            holder.linear.setBackgroundColor(context.getResources().getColor(R.color.unanswered));
        }
        //holder.status.setText(ques.STATUS);

        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AppSharedPreference asp = AppSharedPreference.getInstance(context);
                asp.insertQuestion(ques);
                context.startActivity(new Intent(context, Answers.class));
               // Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

} 