package shout.hackathon.shout.CommunityData;

/**
 * Created by AlZihad on 11/12/2016.
 */


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import shout.hackathon.shout.Adapter.AnswerAdapter;
import shout.hackathon.shout.DataHandler.Answer;
import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.DataHandler.Question;
import shout.hackathon.shout.Map.Community;
import shout.hackathon.shout.R;
import shout.hackathon.shout.Adapter.QuestionAdapter;
/**
 * Created by AlZihad on 11/11/2016.
 */
public class Answers extends Activity
{
    TextView description,user,date;
    Button submit;
    ListView lv;
    Context context;
    AnswerAdapter myAdapter;
    Question question;
    //  int[] prgmImages={R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female};
    //  String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    ArrayList<Answer> answers = new ArrayList<Answer>();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_answers);
        description = (TextView) findViewById(R.id.description);
        user= (TextView) findViewById(R.id.name);
        date = (TextView) findViewById(R.id.date);
        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        question = asp.getQuestion();
        description.setText(question.DESCRIPTION);
        user.setText(question.USER_NAME);
        date.setText(question.DATES);
        context=this;
        getAnswer();
        lv=(ListView) findViewById(R.id.listView);
        lv.setClickable(true);
        myAdapter =new AnswerAdapter(this, answers);
        lv.setAdapter(myAdapter);
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeInput();
            }
        });

    }

    private void takeInput()
    {
        /*final LinearLayout item = (LinearLayout) findViewById(R.id.linear);
        final View v = View.inflate(getApplicationContext(),R.layout.input,null);
        // CardView play = (CardView) v.findViewById(R.id.play);

        final EditText edit = (EditText) v.findViewById(R.id.text);
        Button save =(Button) v.findViewById(R.id.submit);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  send = edit.getText().toString();
                if (send.length()<9)
                {
                    Toast.makeText(getApplicationContext(),"Too Small input",Toast.LENGTH_LONG).show();
                }
                else
                {
                    addAnswer(question.QUESTION_ID,question.USER_ID,question.DESCRIPTION,question.TIMER,question.DATES,question.USER_NAME);
                }
            }
        });

        item.addView(v);*/
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.input);
        dialog.setTitle("Add answer");

        // set the custom dialog components - text, image and button
        final EditText edit = (EditText) dialog.findViewById(R.id.text);
        Button save =(Button) dialog.findViewById(R.id.submit);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String  send = edit.getText().toString();
                if (send.length()<9)
                {
                    Toast.makeText(getApplicationContext(),"Too Small input",Toast.LENGTH_LONG).show();
                }
                else
                {
                    addAnswer(question.QUESTION_ID,question.USER_ID,edit.getText().toString(),question.TIMER,question.DATES,question.USER_NAME);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    public void addAnswer(final String question_id, final String user_id, final String description, final String time, final String date, final String user_name)
    {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                        if(response.contains("suc"))
                        {
                            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                            myAdapter.notifyDataSetChanged();
                            //startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "addAnswer");
                params.put("QUESTION_ID", question_id);
                params.put("USER_ID",user_id);
                params.put("DESCRIPTION",description);
                params.put("TIME",time);
                params.put("DATE",date);
                params.put("USER_NAME",user_name);
                return params;
            }
        };
        queue.add(postRequest);

    }

    public void getAnswer()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                        //showToast(response.toString());
                        //Log.d("Response", response);
                        try {
                            JSONArray jArray = new JSONArray(response);
                            Toast.makeText(getApplicationContext(),jArray.length()+"  ",Toast.LENGTH_LONG).show();
                            for (int i=0;i<jArray.length();i++)
                            {
                                JSONObject job = jArray.getJSONObject(i);
                                String USER_ID = job.getString("USER_ID");
                                String ANSWER_ID = job.getString("ANSWER_ID");
                                String DESCRIPTION = job.getString("DESCRIPTION");
                                String TIMER= job.getString("TIMER");
                                String DATES= job.getString("DATES");
                                String USER_NAME= job.getString("USER_NAME");
                                String STATUS= job.getString("STATUS");
                                String QUESTION_ID= job.getString("QUESTION_ID");
                                //USER_ID,DESCRIPTION,TIMER,DATES,USER_NAME,STATUS,ANSWER_ID,QUESTION_ID;
                                Answer ans = new Answer( USER_ID, DESCRIPTION,  TIMER,  DATES,   USER_NAME,  STATUS,ANSWER_ID,  QUESTION_ID);
                                answers.add(ans);
                                myAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //showToast(e.toString());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "getAllAnswer");
                params.put("QUESTION_ID", question.QUESTION_ID);
                return params;
            }
        };
        queue.add(postRequest);

    }
}
