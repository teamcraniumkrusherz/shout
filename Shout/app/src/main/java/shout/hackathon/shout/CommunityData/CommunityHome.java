package shout.hackathon.shout.CommunityData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.DataHandler.User;
import shout.hackathon.shout.Map.Community;
import shout.hackathon.shout.R;

/**
 * Created by AlZihad on 11/11/2016.
 */
public class CommunityHome extends Activity
{
    Community community;
    TextView community_name, userCount,notification;
    Button history,submit;
    EditText input;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_home);
         AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        final User user = asp.getUserInfo();
        community = asp.getSelectedCommunity();
        community_name = (TextView) findViewById(R.id.community_name);
        userCount = (TextView) findViewById(R.id.usercount);
        notification = (TextView) findViewById(R.id.notification);

        input = (EditText) findViewById(R.id.input);
        submit= (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
                Date currentLocalTime = cal.getTime();
                SimpleDateFormat date = new SimpleDateFormat("HH:mm a");

                date.setTimeZone(TimeZone.getTimeZone("GMT+6:00"));
                String localTime = date.format(currentLocalTime);
                Date d = new Date();
                CharSequence s  = DateFormat.format("MMMM d, yyyy ", d.getTime());
                addQuestion(community.id,user.USER_ID,input.getText().toString(),localTime,s.toString(),"","",community.name,user.NAME);
            }
        });
        community_name.setText(community.getName());
        history = (Button) findViewById(R.id.history);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Questions.class));
            }
        });
        //userCount.setText(community.ge);
        //notification.setText(community.getName());

    }
    public void addQuestion(final String community_id, final String user_id, final String description, final String time, final String date, final String latitude, final String longitude,final String community_name, final String user_name)
    {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

                        if(response.contains("suc"))
                        {
                            Toast.makeText(getApplicationContext(),response, Toast.LENGTH_LONG).show();
                            //startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("query_name", "addQuestion");
                params.put("COMMUNITY_ID", community_id);
                params.put("USER_ID",user_id);
                params.put("DESCRIPTION",description);
                params.put("TIME",time);
                params.put("DATE",date);
                params.put("LATITUDE",latitude);
                params.put("LONGITUDE",longitude);
                params.put("COMMUNITY_NAME",community_name);
                params.put("USER_NAME",user_name);
                return params;
            }
        };
        queue.add(postRequest);

    }

}