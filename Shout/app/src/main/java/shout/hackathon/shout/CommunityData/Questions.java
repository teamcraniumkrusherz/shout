package shout.hackathon.shout.CommunityData;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.DataHandler.Question;
import shout.hackathon.shout.Map.Community;
import shout.hackathon.shout.R;
import shout.hackathon.shout.Adapter.QuestionAdapter;
/**
 * Created by AlZihad on 11/11/2016.
 */
public class Questions extends Activity
{
    ListView lv;
    Context context;
    QuestionAdapter myAdapter;
    Community community;
  //  int[] prgmImages={R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female,R.mipmap.female};
  //  String [] prgmNameList={"Let Us C","c++","JAVA","Jsp","Microsoft .Net","Android","PHP","Jquery","JavaScript"};
    ArrayList<Question> questions = new ArrayList<Question>();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_questions);

        AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        community = asp.getSelectedCommunity();

        context=this;
        getQuestion();
        lv=(ListView) findViewById(R.id.listView);
        lv.setClickable(true);
        myAdapter =new QuestionAdapter(this, questions);
        lv.setAdapter(myAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Question ques = questions.get(i);
                startActivity(new Intent(context,Answers.class));
            }
        });
        ///getQuestion();
    }

    public void getQuestion()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                        //showToast(response.toString());
                        //Log.d("Response", response);
                        try {
                            JSONArray jArray = new JSONArray(response);
                            for (int i=0;i<jArray.length();i++)
                            {
                                JSONObject job = jArray.getJSONObject(i);
                                String USER_ID = job.getString("USER_ID");
                                String DESCRIPTION = job.getString("DESCRIPTION"); 
                                String TIMER= job.getString("TIMER");
                                String DATES= job.getString("DATES");
                                String LATITUDE= job.getString("LATITUDE");
                                String LONGITUDE= job.getString("LONGITUDE");
                                String COMMUNITY_NAME= job.getString("COMMUNITY_NAME");
                                String USER_NAME= job.getString("USER_NAME");
                                String STATUS= job.getString("STATUS");
                                String COMMUNITY_ID= job.getString("COMMUNITY_ID");
                                String QUESTION_ID= job.getString("QUSETION_ID");
                                Question myQues = new Question(  USER_ID,  DESCRIPTION,  TIMER,  DATES,  LATITUDE,  LONGITUDE,  COMMUNITY_NAME,  USER_NAME,  STATUS,  COMMUNITY_ID,  QUESTION_ID);
                                questions.add(myQues);
                                myAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //showToast(e.toString());
                        }
                    }
                },
                new Response.ErrorListener()
                {


                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),"error",Toast.LENGTH_LONG).show();
                        // error
                       // showToast(error.toString());
                       // Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "getCommunityQuestion");
                params.put("COMMUNITY_ID", community.getId());
                return params;
            }
        };
        queue.add(postRequest);
    }
}
