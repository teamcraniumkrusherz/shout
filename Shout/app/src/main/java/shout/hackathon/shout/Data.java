package shout.hackathon.shout;

/**
 * Created by AlZihad on 11/11/2016.
 */
public class Data
{
    String name;
    int image;
    public Data(String name, int image)
    {
        this.name = name;
        this.image=image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }
}
