package shout.hackathon.shout.DataHandler;

/**
 * Created by AlZihad on 11/10/2016.
 */

/**
 * Created by AlZihad on 8/29/2016.
 */

        import android.content.Context;
        import android.content.SharedPreferences;
        import android.content.SharedPreferences.Editor;
        import android.preference.PreferenceManager;

        import shout.hackathon.shout.Map.Community;

public class AppSharedPreference {
    private static AppSharedPreference mAppSharedPreference;

    private SharedPreferences mSharedPreferences;


    private Editor mEditor;
    private static Context mContext;

    /*
     * Implementing Singleton DP
     */
    private AppSharedPreference() {
        mSharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);
        mEditor = mSharedPreferences.edit();
    }

    public static AppSharedPreference getInstance(Context context) {
        mContext = context;
        if (mAppSharedPreference == null)
            mAppSharedPreference = new AppSharedPreference();
        return mAppSharedPreference;
    }

    // show the emergency number dialogue for the very first time use of the application
    public Boolean firstTimeAccess()
    {
        return mSharedPreferences.getBoolean(Constant.FirstTimeAccess,
                false);
    }
    public void putLogInUserToken(boolean token)
    {
        mEditor.putBoolean(Constant.FirstTimeAccess,token);
        mEditor.commit();
    }
    public String getEmergencyNumber1()
    {
        return mSharedPreferences.getString(Constant.EmergencyNumber1,"null");
    }
    public String getEmergencyNumber2()
    {
        return mSharedPreferences.getString(Constant.EmergencyNumber2,"null");
    }
    public void putEmergencyNumber1(String s)
    {
        mEditor.putString(Constant.EmergencyNumber1,s);
        mEditor.commit();
        //return mSharedPreferences.getString(Constant.EmergencyNumber1,"null");
    }
    public void putEmergencyNumber2(String s)
    {
        mEditor.putString(Constant.EmergencyNumber2,s);
        mEditor.commit();
    }

    public String getEmergencySMS()
    {
        return mSharedPreferences.getString(Constant.EmergencySMS,"Help me! I'm in danger. \n#ShakePatch");
    }
    public void putEmergencySMS(String s)
    {
        mEditor.putString(Constant.EmergencySMS,s);
        mEditor.commit();
    }

    public boolean getAdditionalSett()
    {
        return mSharedPreferences.getBoolean(Constant.AddSet,false);
    }
    public void putAdditionalSett(boolean s)
    {
        mEditor.putBoolean(Constant.AddSet,s);
        mEditor.commit();
    }

    public void putShowcaseValueInEmergencyContact()
    {
        mEditor.putBoolean("EmergecyShowcase",true);
        mEditor.commit();
    }

    public boolean getShowcaseValueInEmergencyContact()
    {
        return mSharedPreferences.getBoolean("EmergecyShowcase",false);
    }

    public void putUserId(String id)
    {
        mEditor.putString("USERID",id);
        mEditor.commit();
    }
    public String getUserId()
    {
        return mSharedPreferences.getString("USERID","null");
    }



    public void VideoToPlay(String s)
    {
        mEditor.putString("current",s);
        mEditor.commit();
    }

    public String GetVideoToPlay()
    {
        return mSharedPreferences.getString("current","hrdqtUGT4Rs");
    }

    public void VideoArtistName(String s)
    {
        mEditor.putString("artist",s);
        mEditor.commit();
    }

    public String GetVideoArtistName()
    {
        return mSharedPreferences.getString("artist","Error");
    }

    public void putimageLink(String s)
    {
        mEditor.putString("imageLink",s);
        mEditor.commit();
    }

    public String getimageLink()
    {
        return mSharedPreferences.getString("imageLink","error");
    }

    public void addUserInfo(String USER_ID,String  COMMUNITY_ID,String  COMMUNITY_NAME,String  NAME,String  SEX,String  AGE,String  EMAIL,String  PASSWORD,String  STAR)
    {
        mEditor.putString("AGE",AGE);
        mEditor.putString("USER_ID",USER_ID);
        mEditor.putString("COMMUNITY_ID",COMMUNITY_ID);
        mEditor.putString("COMMUNITY_NAME",COMMUNITY_NAME);
        mEditor.putString("NAME",NAME);
        mEditor.putString("SEX",SEX);
        mEditor.putString("AGE",AGE);
        mEditor.putString("STAR",STAR);
        mEditor.putString("EMAIL",EMAIL);
        mEditor.putString("PASSWORD",PASSWORD);
        mEditor.commit();
    }
    
    public User getUserInfo()
    {
        String USER_ID, COMMUNITY_ID, COMMUNITY_NAME, NAME, SEX, AGE, EMAIL, PASSWORD, STAR;
        String err = "Error";
        AGE = mSharedPreferences.getString("AGE",err);
        USER_ID = mSharedPreferences.getString("USER_ID",err);
        COMMUNITY_ID=mSharedPreferences.getString("COMMUNITY_ID",err);
        COMMUNITY_NAME=mSharedPreferences.getString("COMMUNITY_NAME",err);
        NAME =mSharedPreferences.getString("NAME",err);
        SEX=mSharedPreferences.getString("SEX",err);
        AGE=mSharedPreferences.getString("AGE",err);
        STAR=mSharedPreferences.getString("STAR",err);
        EMAIL=mSharedPreferences.getString("EMAIL",err);
        PASSWORD=mSharedPreferences.getString("PASSWORD",err);
        User user = new User(USER_ID, COMMUNITY_ID, COMMUNITY_NAME, NAME, SEX, AGE, EMAIL, PASSWORD, STAR);
        return user;
    }

    public void selectedCommunity(Community community)
    {
        mEditor.putString("COMMUNITY_ID_COMM",community.getId());
        mEditor.putString("LATITUDE_COMM",community.getLatitude());
        mEditor.putString("LONGITUDE_COMM",community.getLongitude());
        mEditor.putString("LOCATION_COMM",community.getLocation());
        mEditor.putString("NAME_COMM",community.getName());
        mEditor.commit();
    }
    public Community getSelectedCommunity()
    {
        String err ="Error";
        String name,id,location,latitude,longitude;
        name=mSharedPreferences.getString("NAME_COMM",err);
        id=mSharedPreferences.getString("COMMUNITY_ID_COMM",err);
        location=mSharedPreferences.getString("NAME_COMM",err);
        latitude=mSharedPreferences.getString("NAME_COMM",err);
        longitude=mSharedPreferences.getString("NAME_COMM",err);
        Community comm = new Community(name,id,location,latitude,longitude);
        return comm;
    }

    public void insertQuestion(Question ques)
    {
        mEditor.putString("QUES_ID",ques.QUESTION_ID);
        mEditor.putString("USER_QUES_ID",ques.USER_ID);
        mEditor.putString("DESCRIPTION",ques.DESCRIPTION);
        mEditor.putString("TIMER",ques.TIMER);
        mEditor.putString("DATES",ques.DATES);
        mEditor.putString("LATITUDE",ques.LATITUDE);
        mEditor.putString("LONGITUDE",ques.LONGITUDE);
        mEditor.putString("COMMUNITY_QUES_NAME",ques.COMMUNITY_NAME);
        mEditor.putString("USER_NAME",ques.USER_NAME);
        mEditor.putString("STATUS",ques.STATUS);
        mEditor.putString("COMMUNITY_QUES_ID",ques.COMMUNITY_ID);
        mEditor.commit();
    }

    public Question getQuestion()
    {
        String USER_ID,DESCRIPTION,TIMER,DATES,LATITUDE,LONGITUDE,COMMUNITY_NAME,USER_NAME,STATUS,COMMUNITY_ID,QUESTION_ID;
        String err ="Error";
        QUESTION_ID =mSharedPreferences.getString("QUES_ID",err);
        USER_ID=mSharedPreferences.getString("USER_QUES_ID",err);
        DESCRIPTION=mSharedPreferences.getString("DESCRIPTION",err);
        TIMER=mSharedPreferences.getString("TIMER",err);
        DATES=mSharedPreferences.getString("DATES",err);
        LATITUDE=mSharedPreferences.getString("LATITUDE",err);
        LONGITUDE=mSharedPreferences.getString("LONGITUDE",err);
        COMMUNITY_NAME=mSharedPreferences.getString("COMMUNITY_QUES_NAME",err);
       USER_NAME =mSharedPreferences.getString("USER_NAME",err);
        STATUS=mSharedPreferences.getString("STATUS",err);
        COMMUNITY_ID=mSharedPreferences.getString("COMMUNITY_QUES_ID",err);
        Question ques = new Question(USER_ID,DESCRIPTION,TIMER,DATES,LATITUDE,LONGITUDE,COMMUNITY_NAME,USER_NAME,STATUS,COMMUNITY_ID,QUESTION_ID);
        return ques;
    }
}

