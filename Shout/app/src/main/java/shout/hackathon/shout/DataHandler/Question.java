package shout.hackathon.shout.DataHandler;

/**
 * Created by AlZihad on 11/11/2016.
 */
public class Question
{

    public String USER_ID,DESCRIPTION,TIMER,DATES,LATITUDE,LONGITUDE,COMMUNITY_NAME,USER_NAME,STATUS,COMMUNITY_ID,QUESTION_ID;

    public Question(String USER_ID, String DESCRIPTION, String TIMER, String DATES, String LATITUDE, String LONGITUDE, String COMMUNITY_NAME, String USER_NAME, String STATUS, String COMMUNITY_ID, String QUESTION_ID)
    {
        this.COMMUNITY_ID = COMMUNITY_ID;
        this.COMMUNITY_NAME = COMMUNITY_NAME;
        this.DATES = DATES;
        this.DESCRIPTION = DESCRIPTION;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.COMMUNITY_NAME = COMMUNITY_NAME;
        this.STATUS = STATUS;
        this.QUESTION_ID=QUESTION_ID;
        this.USER_NAME = USER_NAME;
        this.USER_ID = USER_ID;
        this.TIMER=TIMER;
    }

}
