package shout.hackathon.shout.Fragments;

/**
 * Created by AlZihad on 11/10/2016.
 */

/**
 * Created by AlZihad on 11/6/2016.
 */


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import shout.hackathon.shout.Adapter.HomeGridAdapter;
import shout.hackathon.shout.CommunityData.CommunityHome;
import shout.hackathon.shout.CommunityData.Questions;
import shout.hackathon.shout.Data;
import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.DataHandler.User;
import shout.hackathon.shout.Home;
import shout.hackathon.shout.LockScreen.LockChangeConfirm;
import shout.hackathon.shout.LockScreen.LockScreenSetup;
import shout.hackathon.shout.R;
import shout.hackathon.shout.maps;


public class UserHome extends Fragment {

    User user;
    ImageView map;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.user_home, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Home");

        AppSharedPreference asp = AppSharedPreference.getInstance(getContext());
        user = asp.getUserInfo();

        TextView name, communityname, location;
        ImageView map;
        name = (TextView) view.findViewById(R.id.name);
        communityname = (TextView) view.findViewById(R.id.communityname);
        location = (TextView) view.findViewById(R.id.location);

        map = (ImageView) view.findViewById(R.id.map);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(),maps.class));
            }
        });
        name.setText(user.NAME);
        communityname.setText(user.COMMUNITY_NAME);
        location.setText("Star Status: "+user.STAR);
        addFirebase();
        GridView gridView = (GridView)view.findViewById(R.id.gridview);
        ArrayList<Data> books = new ArrayList<Data>();
        books.add(new Data("Lock",R.mipmap.lock));
        books.add(new Data("Notification",R.mipmap.notification));
        books.add(new Data("Settings",R.mipmap.settings));
        HomeGridAdapter booksAdapter = new HomeGridAdapter(getContext(), books);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                showToast("hi: "+ i);
                if (i==1)
                {
                    startActivity(new Intent(getActivity(),maps.class));
                }

                if (i==0)
                {
                    startActivity(new Intent(getActivity(),CommunityHome.class));
                }

                if (i==2)
                {
                    startActivity(new Intent(getActivity(),LockScreenSetup.class));
                }
            }
        });
        gridView.setAdapter(booksAdapter);
       /* final Button lock = (Button) view.findViewById(R.id.text);
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                YoYo.with(Techniques.RubberBand)
                        .duration(200)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {

                            @Override
                            public void onAnimationStart(Animator animation)
                            {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation)
                            {
                                SharedPreferences settings = getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
                                String state = settings.getString("available","no");
                                if (state.equals("no")) {
                                    Intent intent = new Intent(getActivity(), LockScreenSetup.class);
                                    startActivity(intent);
                                }
                                else {
                                    startActivity(new Intent(getContext(), LockChangeConfirm.class));
                                }
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {}

                            @Override
                            public void onAnimationRepeat(Animator animation) {}
                        }).playOn(lock);
            }
        });*/

    }

    public void showToast(String text)
    {
        Toast.makeText(getContext(),text,Toast.LENGTH_LONG).show();
    }

   /// Toast.makeText(getApplicationContext(), FirebaseInstanceId.getInstance().getToken()+"hii",Toast.LENGTH_LONG).show();
    public void addFirebase()
    {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        Toast.makeText(getContext(),response,Toast.LENGTH_LONG).show();

                        if(response.contains("suc"))
                        {
                            Toast.makeText(getContext(),response, Toast.LENGTH_LONG).show();
                            //startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("query_name", "updateFirebase");
                params.put("FIREBASE", FirebaseInstanceId.getInstance().getToken());
                params.put("USER_ID",user.USER_ID);
                return params;
            }
        };
        queue.add(postRequest);
    }

}
