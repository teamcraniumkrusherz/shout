package shout.hackathon.shout;

/**
 * Created by AlZihad on 11/11/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.DataHandler.User;

public class Login extends Activity {

    ///ArrayList<Community> community = new ArrayList<Community>();
    final int[] flag = {0};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

       /* AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
        User user = asp.getUserInfo();
        if (!stringMatch(user.NAME,"Error"))
        {
            Toast.makeText(getApplicationContext(),"Already logged in",Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(),Home.class));
        }*/
        final EditText email=(EditText)findViewById(R.id.email);
        final EditText pass = (EditText)findViewById(R.id.pass);
        email.setVisibility(View.GONE);
        pass.setVisibility(View.GONE);

        final EditText name=(EditText)findViewById(R.id.name);
        name.setVisibility(View.GONE);

        final EditText new_email=(EditText)findViewById(R.id.email_new);
        new_email.setVisibility(View.GONE);

        final EditText new_pass=(EditText)findViewById(R.id.new_pass);
        new_pass.setVisibility(View.GONE);

        final Spinner gender =(Spinner)findViewById(R.id.gender);
        gender.setVisibility(View.GONE);

        final EditText age =(EditText)findViewById(R.id.age);
        age.setVisibility(View.GONE);

        final Button login=(Button)findViewById(R.id.login);
        login.setVisibility(View.VISIBLE);
        final Button signup=(Button)findViewById(R.id.signup);
        signup.setVisibility(View.VISIBLE);

        final Spinner comm=(Spinner)findViewById(R.id.comm);
        comm.setVisibility(View.GONE);
        final TextView gen = (TextView)findViewById(R.id.gen);
        gen.setVisibility(View.GONE);


        final TextView commu = (TextView)findViewById(R.id.commu);
        commu.setVisibility(View.GONE);

        final Button submit = (Button)findViewById(R.id.submit);
        submit.setVisibility(View.GONE);


        final int[] state = {0};


        login.setOnClickListener(new View.OnClickListener(){
                                     @Override
                                     public void onClick(View view) {

                                         flag[0] =1;
                                         if(state[0] ==0)
                                         { signup.setVisibility(View.GONE);
                                             email.setVisibility(View.VISIBLE);
                                             pass.setVisibility(View.VISIBLE);
                                             state[0] =1;

                                         }
                                         else if(state[0] ==1)
                                         {

                                             String emai=email.getText().toString();
                                             String pas=pass.getText().toString();
                                             //Toast.makeText(getApplicationContext(),"AAA" , Toast.LENGTH_SHORT).show();
                                             logins(emai, pas);
                                         }



                                         //login//



                                     }
                                 }
        );

        signup.setOnClickListener(new View.OnClickListener(){
                                      @Override
                                      public void onClick(View view) {

                                          flag[0] =1;
                                          login.setVisibility(View.GONE);
                                          signup.setVisibility(View.GONE);

                                          name.setVisibility(View.VISIBLE);
                                          new_email.setVisibility(View.VISIBLE);
                                          new_pass.setVisibility(View.VISIBLE);
                                          gender.setVisibility(View.VISIBLE);
                                          age.setVisibility(View.VISIBLE);
                                          comm.setVisibility(View.VISIBLE);
                                          gen.setVisibility(View.VISIBLE);
                                          commu.setVisibility(View.VISIBLE);
                                          submit.setVisibility(View.VISIBLE);

                                      }

                                  }
        );


        submit.setOnClickListener(new View.OnClickListener(){
                                      @Override
                                      public void onClick(View view) {

                                          String nam=name.getText().toString();
                                          String emails=new_email.getText().toString();
                                          String pas=new_pass.getText().toString();
                                          String sex= gender.getSelectedItem().toString();
                                          String ages = age.getText().toString();
                                          String community_nam=comm.getSelectedItem().toString();
                                          int  community_id= comm.getSelectedItemPosition();
                                          String  commm_id=community_id+"";
                                          signUp(emails,pas,nam,sex,ages,commm_id,community_nam);
                                      }

                                  }
        );


    }


    public void logins(final String emial, final String pass )
    {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response)
                    {

                        Log.d("Response", response);
                         Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                        try {
                            JSONArray jArray = new JSONArray(response);
                            // Toast.makeText(getApplicationContext(),response , Toast.LENGTH_SHORT).show();
                            for (int i=0;i<jArray.length();i++)
                            {
                                JSONObject job = jArray.getJSONObject(i);

                                String u_id = job.getString("USER_ID");
                                String community_id= job.getString("COMMUNITY_ID");
                                String community_name = job.getString("COMMUNITY_NAME");
                                String name = job.getString("NAME");
                                String sex = job.getString("SEX");
                                String age = job.getString("AGE");
                                String Email = job.getString("EMAIL");
                                String Pass = job.getString("PASSWORD");
                                String star = job.getString("STAR");
                                AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                                asp.addUserInfo(u_id,community_id,community_name,name,sex,age,Email,Pass,star);

                            }
                            if(jArray.length()!=0)
                            {
                               startActivity(new Intent(getApplicationContext(), Home.class));
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),"password or email incorrect" , Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "signin");
                params.put("EMAIL", emial);
                params.put("PASSWORD",pass);
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void signUp(final String emails, final String pass, final String name, final String sex, final String age, final String comm_id, final String comm_name)
    {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

                        if(response.contains("suc"))
                        {
                            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), Login.class));
                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "signup");
                params.put("EMAIL", emails);
                params.put("PASSWORD",pass);
                params.put("NAME",name);
                params.put("COMMUNITY_ID",comm_id);
                params.put("COMMUNITY_NAME",name);
                params.put("SEX",sex);
                params.put("AGE",age);
                return params;
            }
        };
        queue.add(postRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(flag[0]==1)
        {
            startActivity(new Intent(getApplicationContext(), Login.class));
            flag[0]--;
        }
        else if(flag[0]==0)
        {
            finish();
        }

    }



    public void getAllAnswers(final String question_id )
    {

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, "http://ghorardim.ml/Shout/appquery.php",
                new Response.Listener<String>()
                {

                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("Response", response);
                        // Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
                        try {
                            JSONArray jArray = new JSONArray(response);
                            // Toast.makeText(getApplicationContext(),response , Toast.LENGTH_SHORT).show();
                            for (int i=0;i<jArray.length();i++)
                            {
                                JSONObject job = jArray.getJSONObject(i);

                                String answer_id = job.getString("ANSWER_ID");
                                String question_Id= job.getString("QUESTION_ID");
                                String user_id = job.getString("USER_ID");
                                String user_name = job.getString("USER_NAME");
                                String description= job.getString("DESCRIPTION");
                                String timer = job.getString("TIMER");
                                String dates = job.getString("DATES");
                                String status = job.getString("STATUS");


                            }
                            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }


                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("query_name", "getAllAnswer");
                params.put("QUESTION_ID",question_id);

                return params;
            }
        };
        queue.add(postRequest);
    }
    public boolean stringMatch(String a, String b) {
        if (a.length() > b.length())
            return false;
        for (int i = 0; i < a.length(); i++)
            if (a.charAt(i) != b.charAt(i))
                return false;
        return true;
    }
}
