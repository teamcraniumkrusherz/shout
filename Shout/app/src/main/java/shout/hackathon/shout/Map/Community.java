package shout.hackathon.shout.Map;

/**
 * Created by AlZihad on 11/12/2016.
 */
public class Community
{
    public String name,id,location,latitude,longitude;
    public Community(String name, String id, String location, String latitude, String longitude)
    {
        this.name = name;
        this.id= id;
        this.location=location;
        this.latitude=latitude;
        this.location=location;
        this.longitude=longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLocation() {
        return location;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}