package shout.hackathon.shout.Map;

/**
 * Created by AlZihad on 11/12/2016.
 */

/**
 * Created by Mai Thanh Hiep on 4/3/2016.
 */
public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
