package shout.hackathon.shout.Services;

/**
 * Created by AlZihad on 11/12/2016.
 */

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import shout.hackathon.shout.Home;
import shout.hackathon.shout.R;

/**
 * Created by AlZihad on 10/30/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService
{

    private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
//        Toast.makeText(getApplicationContext(),remoteMessage.getNotification().getBody(),Toast.LENGTH_LONG).show();
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Home.mBroadcastStringAction);
        broadcastIntent.putExtra("Data", remoteMessage.getNotification().getBody());
        sendBroadcast(broadcastIntent);
    }
}

