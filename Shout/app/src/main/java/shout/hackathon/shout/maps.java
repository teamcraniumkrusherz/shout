package shout.hackathon.shout;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import shout.hackathon.shout.CommunityData.CommunityHome;
import shout.hackathon.shout.DataHandler.AppSharedPreference;
import shout.hackathon.shout.Map.Community;
import shout.hackathon.shout.Map.DirectionFinder;
import shout.hackathon.shout.Map.DirectionFinderListener;
import shout.hackathon.shout.Map.Route;


public class maps extends FragmentActivity implements OnMapReadyCallback, DirectionFinderListener {

    private Marker dipu;
    LatLng dipu_latlong = new LatLng(23.812547,90.425943);


    ArrayList<Community> mapList;
    Marker gp_house;
    Marker GP,IUB,NSU,DU,BUET,BAS;
    private GoogleMap mMap;
    private Button btnFindPath;
    private EditText etOrigin;
    private EditText etDestination;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapactivity);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

       /* mapList = new ArrayList<Community>();
       /// markerList = new ArrayList<Marker>();



        //"1"	"IUB_Changemakers"	" Bashundhara R/A"	"23.815867"	"90.427574"
        mapList.add(data);
        mapList.add(data1);
        mapList.add(data2);
        mapList.add(data3);
        mapList.add(data4);

       //
        //=mMap.addMarker(new MarkerOptions().position(iub).title("IUB"));
        //NSU=mMap.addMarker(new MarkerOptions().position(nsu).title("NSU"));
        /*mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker arg0) {
                if(arg0.getTitle().equals("IUB_Changemakers")) {// if marker source is clicked
                   // Toast.makeText(getApplicationContext(), "Clicked NSU", Toast.LENGTH_LONG).show();
                    sendRequest("23.812680,90.425879",data.getLatitude()+","+data.getLongitude());
                }
                if(arg0.getTitle().equals("NSU_Changemakers")) // if marker source is clicked
                {
                    //Toast.makeText(getApplicationContext(), "Clicked IUB", Toast.LENGTH_LONG).show();
                    sendRequest("23.812680,90.425879",data.getLatitude()+","+data.getLongitude());
                }
                if(arg0.getTitle().equals("DU_Changemakers"))
                {

                    sendRequest("23.812680,90.425879",data.getLatitude()+","+data.getLongitude());
                }
                if(arg0.getTitle().equals("BUET_Changemakers")) // if marker source is clicked
                {
                   // Toast.makeText(getApplicationContext(), "Clicked IUB", Toast.LENGTH_LONG).show();
                    sendRequest("23.812680,90.425879",data.getLatitude()+","+data.getLongitude());
                }
                if(arg0.getTitle().equals("Boshundhara_Community")) {// if marker source is clicked
                    //Toast.makeText(getApplicationContext(), "Clicked NSU", Toast.LENGTH_LONG).show();
                    sendRequest("23.812680,90.425879",data.getLatitude()+","+data.getLongitude());

                }
                return true;
            }
        });*/


        btnFindPath = (Button) findViewById(R.id.btnFindPath);
        //etOrigin = (EditText) findViewById(R.id.etOrigin);
        //etDestination = (EditText) findViewById(R.id.etDestination);

        btnFindPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getApplicationContext(),CommunityHome.class));
            }
        });
        btnFindPath.setVisibility(View.GONE);
    }

    private void sendRequest(String origin, String destination)
    {
        //String origin = "23.812680,90.425879";
        //String destination = etDestination.getText().toString();
        if (origin.isEmpty()) {
            //Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destination.isEmpty()) {
            //Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            new DirectionFinder(this, origin, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng hcmus = new LatLng(23.812038, 90.425994);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hcmus, 18));
        originMarkers.add(mMap.addMarker(new MarkerOptions()
                .title("Gp House")
                .position(hcmus)));
        //gp_house=mMap.addMarker(new MarkerOptions().position(gp).title("GP").snippet("GP_house"));
        //IUB =mMap.addMarker(new MarkerOptions().position(iub).title("IUB_Changemakers").snippet("IUB_Changemakers"));
        /*NSU =mMap.addMarker(new MarkerOptions().position(nsu).title("NSU_Changemakers").snippet("NSU_Changemakers"));
        DU =mMap.addMarker(new MarkerOptions().position(du).title("DU_Changemakers").snippet("DU_Changemakers"));
        BUET =mMap.addMarker(new MarkerOptions().position(buet).title("BUET_Changemakers").snippet("BUET_Changemakers"));
        BAS =mMap.addMarker(new MarkerOptions().position(bas).title("Boshundhara_Community").snippet("Boshundhara_Community"));*/

        final Community data = new Community("IUB_Changemakers","1","Bashundhara R/A","23.815867"	,"90.427574");
        final Community data1 = new Community("NSU_Changemakers","2",	" Bashundhara R/A",	"23.815123",	"90.425642");
        final Community data2 = new Community("DU_Changemakers","3",	"TSC, Nilkhet Road"	,"23.732089",	"90.395801");
        final Community data3 = new Community("BUET_Changemakers","4"	,	"Polashi Mor",	"23.728100",	"90.391894");
        final Community data4 = new Community("Boshundhara_Community","5",	"Road 06",	"23.814306",	"90.428245");

        final LatLng iub = new LatLng(23.815867	,90.427574);
        final LatLng nsu = new LatLng(23.815123,	90.425642);
        final LatLng du = new LatLng(23.732089,	90.395801);
        final LatLng buet = new LatLng(23.728100,	90.391894);
        final LatLng bas = new LatLng(23.814306,	90.428245);



        IUB =mMap.addMarker(new MarkerOptions().position(iub).title("IUB_Changemakers").snippet("IUB_Changemakers"));
        NSU =mMap.addMarker(new MarkerOptions().position(nsu).title("NSU_Changemakers").snippet("NSU_Changemakers"));
        DU =mMap.addMarker(new MarkerOptions().position(du).title("DU_Changemakers").snippet("DU_Changemakers"));
        BUET =mMap.addMarker(new MarkerOptions().position(buet).title("BUET_Changemakers").snippet("BUET_Changemakers"));
        BAS =mMap.addMarker(new MarkerOptions().position(bas).title("Boshundhara_Community").snippet("Boshundhara_Community"));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker arg0) {
                if(arg0.getTitle().equals("IUB_Changemakers")) {// if marker source is clicked
                    AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                    asp.selectedCommunity(data);
                    sendRequest("23.812680,90.425879",data.getLatitude()+","+data.getLongitude());
                    btnFindPath.setVisibility(View.VISIBLE);
                }
                if(arg0.getTitle().equals("NSU_Changemakers")) // if marker source is clicked
                {
                    AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                    asp.selectedCommunity(data1);
                    //Toast.makeText(getApplicationContext(), "Clicked IUB", Toast.LENGTH_LONG).show();
                    sendRequest("23.812680,90.425879",data1.getLatitude()+","+data1.getLongitude());
                    btnFindPath.setVisibility(View.VISIBLE);
                }
                if(arg0.getTitle().equals("DU_Changemakers"))
                {
                    AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                    asp.selectedCommunity(data2);
                    sendRequest("23.812680,90.425879",data2.getLatitude()+","+data2.getLongitude());
                    btnFindPath.setVisibility(View.VISIBLE);
                }
                if(arg0.getTitle().equals("BUET_Changemakers")) // if marker source is clicked
                {
                    // Toast.makeText(getApplicationContext(), "Clicked IUB", Toast.LENGTH_LONG).show();
                    AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                    asp.selectedCommunity(data3);
                    sendRequest("23.812680,90.425879",data3.getLatitude()+","+data3.getLongitude());
                    btnFindPath.setVisibility(View.VISIBLE);
                }
                if(arg0.getTitle().equals("Boshundhara_Community")) {// if marker source is clicked
                    //Toast.makeText(getApplicationContext(), "Clicked NSU", Toast.LENGTH_LONG).show();
                    AppSharedPreference asp = AppSharedPreference.getInstance(getApplicationContext());
                    asp.selectedCommunity(data4);
                    sendRequest("23.812680,90.425879",data4.getLatitude()+","+data4.getLongitude());
                    btnFindPath.setVisibility(View.VISIBLE);

                }
                return true;
            }
        });
        mMap.setMyLocationEnabled(true);
    }


    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            ((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            ((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }
}
